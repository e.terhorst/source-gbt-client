import Vue from "vue";
import Router from "vue-router";

import store from "@/store";
import Products from "@/components/product/Products";
import Register from "@/components/register/Register";
import Login from "@/components/auth/Login"
import Logout from "@/components/auth/Logout";
import Configuration from "@/components/configuration/Configuration";

Vue.use(Router)

const ifNotAuthenticated = (to, from, next) => {
    if(!store.getters.isAuthenticated) {
        next()
        return
    }
    next("/")
}

const ifAuthenticated = (to, from, next) => {
    if(store.getters.isAuthenticated) {
        next()
        return
    }
    next("/login");
}

const hasStartedConfiguration = (to, from, next) => {
    if(store.getters.isAuthenticated && store.getters.isConfigurationCreated) {
        next()
        return
    }
    next("/products");
}

export default new Router({
    mode: "history",
    routes: [
        {
            path: "/",
            alias: "/products",
            name: "products",
            component: Products,
            beforeEnter: ifAuthenticated
        },
        {
            path: "/configuration",
            name: "configuration",
            component: Configuration,
            beforeEnter: hasStartedConfiguration
        },
        {
            path: "/login",
            name: "login",
            component: Login,
            beforeEnter: ifNotAuthenticated
        },
        {
            path: "/logout",
            name: "logout",
            component: Logout,
            beforeEnter: ifAuthenticated
        },
        {
            path: "/register",
            name: "register",
            component: Register,
            beforeEnter: ifNotAuthenticated
        },
    ]
});

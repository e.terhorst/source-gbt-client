import {
  CONF_CREATE_REQUEST,
  CONF_CREATE_SUCCESS,
  CONF_ANSWERS_REQUEST,
  CONF_ANSWER_SUBMIT_REQUEST,
  CONF_ANSWER_SET,
  CONF_ERROR,
  CONF_ANSWER_SUBMIT_SUCCESS,
  CONF_COMPLETE_REQUEST,
  CONF_COMPLETE_SUCCESS,
  CONF_PRICE_REQUEST,
  CONF_PRICE_SUCCESS,
  CONF_PRICE_ERROR,
} from "@/store/actions/configuration";
import axios from "axios";

const state = {
  status: null,
  error: null,
  configurationId: null,
  answers: {},
  modifiedQuestionSetReference: null,
  price: null,
  priceStatus: null
}

const getters = {
  isConfigurationCreatePending: state => state.status === 'pending',
  isConfigurationCreated: state => !!state.configurationId,
  isConfigurationCompletePending: state => state.status === 'completing',
  isConfigurationCompleted: state => state.status === 'completed',
  hasConfigurationError: state => state.error,
  configurationId: state => state.configurationId,
  getAnswers: state => state.answers,
  getQuestionSetAnswers: (state) => (questionSetReference) => {
    return state.answers[questionSetReference]
  },
  getAnswerForQuestion: (state) => (questionSetReference, questionReference) => {
    return state.answers[questionSetReference][questionReference]
  },
  isQuestionSetAnswered: (state) => {
    return (questionSetReference) => {
      let questionSetAnswers = state.answers[questionSetReference] // evaluate > error otherwise
      let keys = Object.keys(questionSetAnswers);
      for(let i = 0; i < keys.length; i++) {
        if (questionSetAnswers[keys[i]] === null)
          return false
      }
      return true
    }
  },
  isQuestionSetModified: (state) => (questionSetReference) => {
    return state.modifiedQuestionSetReference === questionSetReference
  },
  hasConfigurationPriceError: (state) => state.priceStatus === 'error',
  getConfigurationPrice: (state) => state.price
}

const actions = {
  [CONF_CREATE_REQUEST]: ({commit}, data) => {
    return new Promise((resolve, reject) => {
      commit(CONF_CREATE_REQUEST)
      axios({
        url: '/configuration/',
        method: 'post',
        data: {
          product_reference: data.productReference
        }
      }).then(response => {
        commit(CONF_CREATE_SUCCESS, response.data.id)
        resolve(response)
      }).catch(error => {
        commit(CONF_ERROR, error)
        reject(error)
      })
    })
  },

  [CONF_ANSWERS_REQUEST]: ({commit}) => {
    return new Promise((resolve, reject) => {
      commit(CONF_ANSWERS_REQUEST)
      axios({
        url: `/configuration/${state.configurationId}/answers/`,
        method: 'get'
      }).then(response => {
        commit(CONF_ANSWERS_REQUEST, response.data)
        resolve(response)
      }).catch(error => {
        commit(CONF_ERROR)
        reject(error)
      })
    })
  },
  [CONF_ANSWER_SET]: ({commit}, data) => {
    commit(CONF_ANSWER_SET, data)
  },
  [CONF_ANSWER_SUBMIT_REQUEST]: ({commit}, data) => {
    return new Promise((resolve, reject) => {
      commit(CONF_ANSWER_SUBMIT_REQUEST)
      axios({
        url: `/configuration/${state.configurationId}/answers/`,
        method: 'post',
        data: data
      }).then(response => {
        commit(CONF_ANSWER_SUBMIT_SUCCESS, response.data)
        resolve(response)
      }).catch(error => {
        // TODO: Should not raise global error => Field validation errors
        commit(CONF_ERROR, error)
        reject(error)
      })
    })
  },

  [CONF_PRICE_REQUEST]: ({commit}) => {
    return new Promise((resolve, reject) =>{
      commit(CONF_PRICE_REQUEST)
      axios({
        url: `/configuration/${state.configurationId}/price/`,
        method: 'get'
      }).then(response => {
        commit(CONF_PRICE_SUCCESS, response.data.price)
        resolve(response)
      }).catch(error => {
        commit(CONF_PRICE_ERROR, error)
        reject(error)
      })
    })
  },

  [CONF_COMPLETE_REQUEST]: ({commit}) => {
    return new Promise((resolve, reject) => {
      commit(CONF_COMPLETE_REQUEST)
      axios({
        url: `/configuration/${state.configurationId}/complete/`,
        method: 'post',
        data: {}
      }).then(response => {
        commit(CONF_COMPLETE_SUCCESS)
        resolve(response)
      }).catch(error => {
        commit(CONF_ERROR, error)
        reject(error)
      })
    })
  }
}

const mutations = {
  [CONF_CREATE_REQUEST]: state => {
    state.status = 'pending'
    // Make sure everything has been reset.
    state.error = null
    state.configurationId = null
    state.answers = {}
    state.modifiedQuestionSetReference = null
    state.submitAnswersState = null
    state.priceStatus = null
    state.price = null
  },
  [CONF_CREATE_SUCCESS]: (state, configurationId) => {
    state.status = 'created'
    state.error = ''
    state.configurationId = configurationId
  },

  [CONF_ANSWER_SET]: (state, data) => {
    state.answers[data.questionSetReference][data.questionReference] = data.answer
    state.modifiedQuestionSetReference = data.questionSetReference
  },
  [CONF_ANSWERS_REQUEST]: (state, answers) => {
    state.answers = answers
  },
  [CONF_ANSWER_SUBMIT_REQUEST]: (state) => {
    state.submitAnswersState = 'pending';
  },
  [CONF_ANSWER_SUBMIT_SUCCESS]: (state, answers) => {
    state.submitAnswersState = 'success';
    state.answers = answers
    state.modifiedQuestionSetReference = null
  },

  [CONF_PRICE_REQUEST]: (state) => {
    state.priceStatus = 'pending'
  },
  [CONF_PRICE_SUCCESS]: (state, price) => {
    state.priceStatus = 'ready'
    state.price = price
  },
  [CONF_PRICE_ERROR]: (state, error) => {
    state.error = error
    state.priceStatus = 'error'
    state.price = null
  },

  [CONF_COMPLETE_REQUEST]: (state) => {
    state.status = 'completing'
  },
  [CONF_COMPLETE_SUCCESS]: (state) => {
    state.status = 'completed'
  },

  [CONF_ERROR]: (state, error) => {
    state.status = ''
    state.error = error
  },
}

export default {
  state,
  getters,
  actions,
  mutations
}

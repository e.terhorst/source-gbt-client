import {REGISTER_REQUEST, REGISTER_SUCCESS, REGISTER_ERROR} from "@/store/actions/register";
import axios from "axios";

const state = {
  status: '',
  context: {},
  error: {}
}

const getters = {
  isRegistrationPending: state => state.status === 'pending',
  isRegistrationCompleted: state => state.status === 'completed',
  registrationContext: state => state.context
}

const actions = {
  [REGISTER_REQUEST]: ({commit}, data) => {
    return new Promise((resolve, reject) => {
      commit(REGISTER_REQUEST)
      axios({
        url: '/user/',
        method: 'post',
        data: {
          email: data.email,
          password: data.password,
          password_confirm: data.password_confirm,
          first_name: data.first_name,
          last_name: data.last_name
        }

      }).then(response => {
        commit(REGISTER_SUCCESS, response.data)
        resolve(response)

      }).catch(error => {
        commit(REGISTER_ERROR)
        reject(error)
      })
    })
  }
}

const mutations = {
  [REGISTER_REQUEST]: state => {
    state.status = 'pending'
    state.user_email = ''
  },
  [REGISTER_SUCCESS]: (state, data) => {
    state.status = 'completed'
    state.context = {
      email: data.email,
      first_name: data.first_name,
      last_name: data.last_name
    }
  },
  [REGISTER_ERROR]: (state) => {
    state.status = 'error'
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}

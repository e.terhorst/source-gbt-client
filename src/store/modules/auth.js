import {AUTH_REQUEST, AUTH_ERROR, AUTH_SUCCESS, AUTH_LOGOUT} from "../actions/auth";
import axios from "axios";

const state = {
  token: localStorage.getItem("user-token") || "",
  status: "",
  error: ""
}

const getters = {
  isAuthenticated: state => !!state.token,
  isAuthenticationPending: state => state.status === 'pending'
}

const actions = {
  [AUTH_REQUEST]: ({commit}, credentials) => {
    return new Promise((resolve, reject) => {
      commit(AUTH_REQUEST);
      axios({
        url: '/auth/',
        method: 'post',
        data: {
          username: credentials.email,
          password: credentials.password
        }

      }).then(response => {
        localStorage.setItem('token', response.data.token)
        axios.defaults.headers.common['Authorization'] = `Token ${response.data.token}`
        commit(AUTH_SUCCESS, response.data.token)
        resolve(response)

      }).catch(error => {
        commit(AUTH_ERROR, error)
        localStorage.removeItem('token')
        reject(error)
      })
    })
  },
  [AUTH_LOGOUT]: ({commit}) => {
    return new Promise(resolve => {
      commit(AUTH_LOGOUT)
      localStorage.removeItem('token')
      resolve()
    })
  }
}

const mutations = {
  [AUTH_REQUEST]: state => {
    state.status = 'pending';
  },
  [AUTH_SUCCESS]: (state, token) => {
    state.status = 'success'
    state.token = token;
  },
  [AUTH_ERROR]: (state, error) => {
    state.status = 'error';
    state.error = error
  },
  [AUTH_LOGOUT]: (state) => {
    state.status = '';
    state.token = ''
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}

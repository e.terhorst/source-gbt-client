import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'

import App from './App.vue'
import store from './store';
import router from './routers';

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(Vuex)
Vue.use(VueAxios, axios)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

Vue.config.productionTip = false

axios.defaults.baseURL = "http://localhost:8100/api/v1"
axios.defaults.headers.common['Content-Type'] = 'application/json'

new Vue({
    el: '#app',
    store,
    router,
    components: { App },
    render: h => h(App),
}).$mount('#app')

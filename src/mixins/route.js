export const routeMixin = {
    methods: {
        routeTo: function (target) {
            if(this.$router.currentRoute.name !== target) {
                this.$router.push(target);
            }
        }
    }
}

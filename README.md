Into The Source : Gordijn Bereken Tool - Client
---

- Author: Edwin ter Horst
- Project: Gordijn Bereken Tool Client ([Source](https://gitlab.com/e.terhorst/source-gbt-client), [Issues](https://gitlab.com/e.terhorst/source-gbt-client/-/issues))
- Goal: Trial project for job-application
- Email: [contact@edwinterhorst.nl](mailto:contact@edwinterhorst.nl?subject=[GitLab]%20Gordijn%20Bereken%20Tool%20Client)


For the server (back-end), see:
- Gordijn Bereken Tool ([Source](https://gitlab.com/e.terhorst/source-gbt), [Issues](https://gitlab.com/e.terhorst/source-gbt/-/issues))

# Running the application

##### Requirements
- [gbt:server](https://gitlab.com/e.terhorst/source-gbt)
- [nodejs](https://nodejs.org/en/download/)
- [npm](https://www.npmjs.com/get-npm)
- [git](https://git-scm.com/downloads)

##### Clone from GitLab
```
$ git clone https://gitlab.com/e.terhorst/source-gbt-client.git
```

##### Project setup
```
npm install
```

##### Compiles and hot-reloads for development
```
npm run serve
```

# TechStack

- [Git](https://www.git-scm.com/) - Source control manager
- [GitLab](https://www.gitlab.com/) - Online dev-ops tool
- [NodeJS](https://nodejs.org/en/) - JavaScript event-driven runtime
- [Node Package Manager](https://docs.npmjs.com/) - Software registry and orchestration
- [VueJS](https://vuejs.org/) - Front-end JavaScript framework
- [VueRouter](https://router.vuejs.org/) - Vue router support
- [BootstrapVue](https://bootstrap-vue.org/) Vue wrapper around the Bootstrap style framework.
- [Axios](https://github.com/axios/axios) - Promise-based HTTP client

#### deafult user logins: 

Only available when fixtures are loaded on server-side.

 - `superuser@edwinterhorst.nl`
 - `staffuser@edwinterhorst.nl`
 - `active_user@edwinterhorst.nl`
 - `inactive_user@edwinterhorst.nl`

#### password for all
 - `test2021
